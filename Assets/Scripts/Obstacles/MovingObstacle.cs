using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObstacle : MonoBehaviour
{
    [SerializeField] private float _travelTime;

    private float _timeCountX = 1000;
    private float _startPositionX;
    private float _targetX;

    public float TravelTime
    {
        get => _travelTime;
        private set => _travelTime = value;
    }

    // Start is called before the first frame update
    void Start()
    {
        _timeCountX = 0;
        _startPositionX = transform.position.x;
        _targetX = -_startPositionX;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _timeCountX += Time.deltaTime / _travelTime;

        Vector3 pos = transform.position;
        pos.x = Mathf.Lerp(_startPositionX, _targetX, _timeCountX);
        transform.position = pos;

        if (transform.position.x == _targetX)
        {
            _timeCountX = 0;
            _startPositionX = transform.position.x;
            _targetX = -_startPositionX; 
        }
    }
}