using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] private float _baseMovement = 0.4f;
    [SerializeField] private float _collisionMovement = 0.1f;
    [SerializeField] private float _currentMovement = 0;
    [SerializeField] private float _moveTime = 0.4f;
    [SerializeField] private float _jumpTime = 2f;
    [SerializeField] private float _maxX = 2.3f;
    [SerializeField] private float _maxY = 3f;

    private float _timeCountX = 1000;
    private float _startPositionX;
    private float _targetX;

    private float _timeCountY = 1000;
    private float _startPositionY;
    private float _targetY;

    public float MoveTime
    {
        get => _moveTime;
        private set => _moveTime = value;
    }

    public float JumpTime
    {
        get => _jumpTime;
        private set => _jumpTime = value;
    }

    public float MaxX
    {
        get => _maxX;
        private set => _maxX = value;
    }

    public float MaxY
    {
        get => _maxY;
        private set => _maxY = value;
    }

    private void Start()
    {
        _currentMovement = _baseMovement;
    }

    private void FixedUpdate()
    {
        Vector3 pos = transform.position;
        pos.z += _currentMovement;

        transform.position = pos;
    }

    /**
     * Moves the player and checks for inputs
     */
    void Update()
    {
        
        
        if (transform.position.y != _targetY)
        {
            _timeCountY += Time.deltaTime / (JumpTime / 2);

            Vector3 pos = transform.position;
            pos.y = Mathf.Lerp(_startPositionY, _targetY, _timeCountY);
            transform.position = pos;
        }
        else
        {
            _timeCountY = 0;
            _startPositionY = transform.position.y;
            _targetY = 0;
        }


        if (transform.position.x != _targetX)
        {
            _timeCountX += Time.deltaTime / MoveTime;

            Vector3 pos = transform.position;
            pos.x = Mathf.Lerp(_startPositionX, _targetX, _timeCountX);
            transform.position = pos;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Move(-1);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Move(1);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }

    /**
     * Checks if the move is within the boundries of the map and set the move variables
     */
    public void Move(int direction)
    {
        if (transform.position.x == _targetX && transform.position.y == _targetY &&
            Mathf.Abs(transform.position.x + _maxX * direction) <= _maxX)
        {
            _timeCountX = 0;
            _startPositionX = transform.position.x;
            _targetX = _startPositionX + _maxX * direction;
        }
    }

    /**
     * Checks if the move is within the boundries of the map and set the move variables
     */
    public void Jump()
    {
        if (transform.position.x == _targetX && transform.position.y == _targetY)
        {
            _timeCountY = 0;
            _startPositionY = transform.position.y;
            _targetY = _startPositionY + _maxY;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("still"))
        {
            _currentMovement = _collisionMovement;
            GetComponent<Player>().LoseLive();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("still"))
        {
            _currentMovement = _baseMovement;
        }
    }
}