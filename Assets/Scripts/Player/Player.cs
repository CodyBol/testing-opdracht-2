using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private int _lives = 3;
    [SerializeField] private int _coins = 0;
    [SerializeField] private int _pickupAmount = 0;
    [SerializeField] private Text _scoreText, _distanceText, _livesText, _pickupsText;

    private float _countdown = 3;

    public int Lives
    {
        get => _lives;
        private set => _lives = value;
    }

    public int Coins
    {
        get => _coins;
        set => _coins = value;
    }

    public void LoseLive()
    {
        _lives--;
        if (_lives == 0)
        {
            GameOver();
        }
    }

    private void Start()
    {
        GetComponent<Movement>().enabled = false;
    }


    private void Update()
    {
        _countdown -= Time.deltaTime;

        if (_countdown <= 0 && _countdown != -1000)
        {
            GetComponent<Movement>().enabled = true;
            _countdown = -1000;
        }

        if (_scoreText != null)
        {
            _scoreText.text = "Score: " + _coins;
            _distanceText.text = "Distance: " + (int) transform.position.z;
            _livesText.text = "Lives: " + _lives;
            _pickupsText.text = "Pickups: " + _pickupAmount;
        }

        if (transform.position.z >= 485)
        {
            GameOver();
            RestartScene();
        }

        if (_lives == 0)
        {
            RestartScene();
        }
    }

    public void GameOver()
    {
        GetComponent<Movement>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("coin"))
        {
            _pickupAmount++;
            _coins = (int) transform.position.z / 10;

            Destroy(other.gameObject);
        }
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(0);
    }
}