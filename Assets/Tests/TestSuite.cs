using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class TestSuite
{
    [UnityTest]
    public IEnumerator MoveRight()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(4f);
        movementComponent.Move(1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == 2.3f);
    }

    [UnityTest]
    public IEnumerator MoveRightDuringCountdown()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);
        movementComponent.Move(1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == 0f);
    }

    [UnityTest]
    public IEnumerator MoveRightTryToGetOutOfMap()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(4f);
        movementComponent.Move(1);
        yield return new WaitForSeconds(0.5f);
        movementComponent.Move(1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == 2.3f);
    }

    [UnityTest]
    public IEnumerator MoveLeft()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(4f);
        movementComponent.Move(-1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == -2.3f);
    }

    [UnityTest]
    public IEnumerator MoveLeftDuringCountdown()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);
        movementComponent.Move(-1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == 0f);
    }

    [UnityTest]
    public IEnumerator MoveLeftTryToGetOutOfMap()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(4f);
        movementComponent.Move(-1);
        yield return new WaitForSeconds(0.5f);
        movementComponent.Move(-1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(player.transform.position.x == -2.3f);
    }

    [UnityTest]
    public IEnumerator PlayerDiesDuringStartCountdown()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 3; i++)
        {
            playerComponent.LoseLive();
        }

        yield return new WaitForSeconds(1f);

        Assert.True(player != GameObject.Find("Player"));
    }

    [UnityTest]
    public IEnumerator MoveRightAfterDeath()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 3; i++)
        {
            playerComponent.LoseLive();
        }

        yield return null;

        yield return new WaitForSeconds(4f);
        GameObject.Find("Player").GetComponent<Movement>().Move(1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(GameObject.Find("Player").transform.position.x == 2.3f && player != GameObject.Find("Player"));
    }

    [UnityTest]
    public IEnumerator MoveRightTryToGetOutOfMapAfterDeath()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 3; i++)
        {
            playerComponent.LoseLive();
        }

        yield return null;

        yield return new WaitForSeconds(4f);
        GameObject.Find("Player").GetComponent<Movement>().Move(1);
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("Player").GetComponent<Movement>().Move(1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(GameObject.Find("Player").transform.position.x == 2.3f && player != GameObject.Find("Player"));
    }

    [UnityTest]
    public IEnumerator MoveLeftAfterDeath()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 3; i++)
        {
            playerComponent.LoseLive();
        }

        yield return null;

        yield return new WaitForSeconds(4f);
        GameObject.Find("Player").GetComponent<Movement>().Move(-1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(GameObject.Find("Player").transform.position.x == -2.3f && player != GameObject.Find("Player"));
    }

    [UnityTest]
    public IEnumerator MoveLeftTryToGetOutOfMapAfterDeath()
    {
        SceneManager.LoadScene(0);

        yield return null;

        GameObject player = GameObject.Find("Player");
        Player playerComponent = player.GetComponent<Player>();
        Movement movementComponent = player.GetComponent<Movement>();

        yield return new WaitForSeconds(1f);

        for (int i = 0; i < 3; i++)
        {
            playerComponent.LoseLive();
        }

        yield return null;

        yield return new WaitForSeconds(4f);
        GameObject.Find("Player").GetComponent<Movement>().Move(-1);
        yield return new WaitForSeconds(0.5f);
        GameObject.Find("Player").GetComponent<Movement>().Move(-1);
        yield return new WaitForSeconds(0.5f);

        Assert.True(GameObject.Find("Player").transform.position.x == -2.3f && player != GameObject.Find("Player"));
    }
}