using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class EditorTestSuite
{
    // A Test behaves as an ordinary method
    [Test]
    public void EditorTestSuiteSimplePasses()
    {
        Assert.True(true);
    }
    
    // A Test behaves as an ordinary method
    [Test]
    public void EditorTestSuiteSimpleFail()
    {
        Assert.True(true);
    }

    // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
    // `yield return null;` to skip a frame.
    [UnityTest]
    public IEnumerator EditorTestSuiteWithEnumeratorPasses()
    {
        // Use the Assert class to test conditions.
        // Use yield to skip a frame.
        yield return null;
        
        Assert.True(true);
    }
}
